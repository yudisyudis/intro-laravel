<!DOCTYPE html>
<html>
    <head>
        <title>Sanberbook</title>
        <meta charset="UTF-8">
    </head>
    
    <body>
        <a href="/home">Home</a>

        <div>
            <h1>Buat Account Baru!</h1>
        </div>

        <!--Isi-->

        <div>
            <h2>Sign Up Form</h2>
            <form action="/welcome" method="POST">
                @csrf
                <h3>Nama Depan</h3>
                <input type="text" name='nama-depan'>
                <h3>Nama Belakang</h3>
                <input type="text" name='nama-belakang'><br>
                <input type="submit" value="Sign Up">
            </form>
        </div>
    </body>
</html>
