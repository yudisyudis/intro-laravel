<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function reg(){
        return view('register');
    }

    public function wel(Request $request){
        //dd($request->all());
        $depan=$request['nama-depan'];
        $belakang=$request['nama-belakang'];
        return view('welcome', compact('depan', 'belakang'));
    }
}
